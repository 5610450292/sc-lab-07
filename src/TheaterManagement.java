import java.util.HashMap;


public class TheaterManagement {
	static double[][] seatMon =DataSeatPrice.ticketPrices1 ;
	static double[][] seatTue =DataSeatPrice.ticketPrices2;
	static double[][] seatWed =DataSeatPrice.ticketPrices3 ;
	static double[][] seatThu =DataSeatPrice.ticketPrices4 ;
	static double[][] seatFri =DataSeatPrice.ticketPrices5 ;
	static double[][] seatSat =DataSeatPrice.ticketPrices6 ;
	static double[][] seatSun =DataSeatPrice.ticketPrices7 ;


	HashMap Hm1 = new HashMap() ;
	HashMap Hm2 = new HashMap() ;
	public void Makerow(String Key, Integer Value){
		Hm1.put(Key, Value) ;
		Hm2.put(Value,Key) ;
	}
	public double BuyticketFromRowCol (String day, String row, int col ){
		double a = 0.0 ;
		if (day.equals("Mon")){
			int r = (int) Hm1.get(row) ;
			a = seatMon[r][col] ;
			seatMon[r][col] = 0 ;
			}
		else if (day.equals("Tue")){
			int r = (int) Hm1.get(row) ;
			a = seatTue[r][col] ;
			seatTue[r][col] = 0 ;
		}
		else if (day.equals("Wed")){
			int r = (int) Hm1.get(row) ;
			a = seatWed[r][col] ;
			seatWed[r][col] = 0 ;
		}
		else if (day.equals("Thu")){
			int r = (int) Hm1.get(row) ;
			a = seatThu[r][col] ;
			seatThu[r][col] = 0 ;
		}
		else if (day.equals("Fri")){
			int r = (int) Hm1.get(row) ;
			a = seatFri[r][col] ;
			seatFri[r][col] = 0 ;
		}
		else if (day.equals("Sat")){
			int r = (int) Hm1.get(row) ;
			a = seatSat[r][col] ;
			seatSat[r][col] = 0 ;
		}
		else if (day.equals("Sun")){
			int r = (int) Hm1.get(row) ;
			a = seatSun[r][col] ;
			seatSun[r][col] = 0 ;
		}


		return a ;
	}
	public String BuyticketFromMoney (String day ,int money){
		String rc = "" ;
		if (day.equals("Mon")){
			for (int r = 0 ; r < seatMon.length ; r++){
				
				for (int c = 0 ; c < seatMon[r].length ; c++){
					int j = c+1 ;
					if (seatMon[r][c] == money){
						rc = rc + "[" + Hm2.get(r) + j + "]";
					}
				}
				
			}

		}
		else if (day.equals("Tue")){
			for (int r = 0 ; r < seatTue.length ; r++){
				
				for (int c = 0 ; c < seatTue[r].length ; c++){
					int j = c+1 ;
					if (seatTue[r][c] == money){
						rc = rc + "[" + Hm2.get(r) + j + "]";
					}
				}
				
			}
		}
		else if (day.equals("Wed")){
			for (int r = 0 ; r < seatWed.length ; r++){
				
				for (int c = 0 ; c < seatWed[r].length ; c++){
					int j = c+1 ;
					if (seatWed[r][c] == money){
						rc = rc + "[" + Hm2.get(r) + j + "]";
					}
				}
				
			}
		}
		else if (day.equals("Thu")){
			for (int r = 0 ; r < seatThu.length ; r++){
				
				for (int c = 0 ; c < seatThu[r].length ; c++){
					int j = c+1 ;
					if (seatThu[r][c] == money){
						rc = rc + "[" + Hm2.get(r) + j + "]";
					}
				}
				
			}
		}
		else if (day.equals("Fri")){
			for (int r = 0 ; r < seatFri.length ; r++){
				
				for (int c = 0 ; c < seatFri[r].length ; c++){
					int j = c+1 ;
					if (seatFri[r][c] == money){
						rc = rc + "[" + Hm2.get(r) + j + "]";
					}
				}
				
			}
		}
		else if (day.equals("Sat")){
			for (int r = 0 ; r < seatSat.length ; r++){
				
				for (int c = 0 ; c < seatSat[r].length ; c++){
					int j = c+1 ;
					if (seatSat[r][c] == money){
						rc = rc + "[" + Hm2.get(r) + j + "]";
					}
				}
				
			}
		}
		else if (day.equals("Sun")){
			for (int r = 0 ; r < seatSun.length ; r++){
				
				for (int c = 0 ; c < seatSun[r].length ; c++){
					int j = c+1 ;
					if (seatSun[r][c] == money){
						rc = rc + "[" + Hm2.get(r) + j + "]";
					}
				}
				
			}
		}

		return rc ;
	}
	public static void main (String[] args){
		TheaterManagement th = new TheaterManagement() ;
		th.Makerow("A", 0);
		th.Makerow("B", 1);
		th.Makerow("C", 2);
		th.Makerow("D", 3);
		th.Makerow("E", 4);
		th.Makerow("F", 5);
		th.Makerow("G", 6);
		th.Makerow("H", 7);
		th.Makerow("I", 8);
		th.Makerow("J", 9);
		th.Makerow("K", 10);
		th.Makerow("L", 11);
		th.Makerow("M", 12);
		th.Makerow("N", 13);
		th.Makerow("O", 14);
		double sum = th.BuyticketFromRowCol("Sun","O",11) ;
		sum += th.BuyticketFromRowCol("Sun","O",12) ;
		
		System.out.println(sum) ;
		System.out.println(th.BuyticketFromMoney("Mon",20)) ;
		System.out.println(th.BuyticketFromMoney("Mon",50)) ;
		double sum2 = th.BuyticketFromRowCol("Fri","O",12) ;
		sum2 += th.BuyticketFromRowCol("Fri","O",13) ;
		System.out.println(sum2) ;
	}
}
