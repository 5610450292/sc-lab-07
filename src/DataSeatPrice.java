
public class DataSeatPrice {
	   public static final double ticketPrices1[][] ={ 
	   { 0 , 0 , 10	, 10 , 10 , 10 , 10 , 10 , 0 , 0 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 0 , 0 } ,
	   { 0 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 0 , 0 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 0 } ,
	   { 10 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 0 , 0 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 10 ,10 } ,
	   { 10 , 10 , 20 , 20 , 20 , 20 , 20 , 20 , 0 , 0 , 20 , 20 , 20 , 20 , 20 , 20 , 20 , 20 , 10 , 10 } ,
	   { 10 , 10 , 20 , 20 , 20 , 20 , 20 , 20 , 0 , 0 , 20 , 20 , 20 , 20 , 20 , 20 , 20 , 20 , 10 , 10 } ,
	   { 10 , 10 , 20 , 20 , 20 , 20 , 20 , 20 , 0 , 0 , 20 , 20 , 20 , 20 , 20 , 20 , 20 , 20 , 10 , 10 } ,
	   { 20 , 20 , 20 , 30 , 30 , 30 , 30 , 30 , 0 , 0 , 30 , 30 , 30 , 30 , 30 , 30 , 30 , 20 , 20 , 20 } ,
	   { 30 , 30 , 30 , 40 , 40 , 40 , 40 , 40 , 0 , 0 , 40 , 40 , 40 , 40 , 40 , 40 , 40 , 30 , 30 , 30 } ,
	   { 30 , 30 , 30 , 40 , 40 , 40 , 40 , 40 , 0 , 0 , 40 , 40 , 40 , 40 , 40 , 40 , 40 , 30 , 30 , 30 } ,
	   { 30 , 30 , 30 , 40 , 40 , 40 , 40 , 40 , 0 , 0 , 40 , 40 , 40 , 40 , 40 , 40 , 40 , 30 , 30 , 30 } ,
	   { 30 , 30 , 30 , 40 , 40 , 40 , 40 , 40 , 0 , 0 , 40 , 40 , 40 , 40 , 40 , 40 , 40 , 30 , 30 , 30 } ,
	   { 30 , 30 , 30 , 40 , 40 , 40 , 40 , 40 , 0 , 0 , 40 , 40 , 40 , 40 , 40 , 40 , 40 , 30 , 30 , 30 } ,
	   { 40 , 40 , 40 , 50 , 50 , 50 , 50 , 50 , 0 , 0 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 40 , 40 , 40 } ,
	   { 40 , 40 , 40 , 50 , 50 , 50 , 50 , 50 , 0 , 0 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 40 , 40 , 40 } ,
	   { 40 , 40 , 40 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 40 , 40 , 40 } };
	   public static final double ticketPrices2[][] ={ 
		   { 0 , 0 , 10	, 10 , 10 , 10 , 10 , 10 , 0 , 0 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 0 , 0 } ,
		   { 0 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 0 , 0 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 0 } ,
		   { 10 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 0 , 0 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 10 ,10 } ,
		   { 10 , 10 , 20 , 20 , 20 , 20 , 20 , 20 , 0 , 0 , 20 , 20 , 20 , 20 , 20 , 20 , 20 , 20 , 10 , 10 } ,
		   { 10 , 10 , 20 , 20 , 20 , 20 , 20 , 20 , 0 , 0 , 20 , 20 , 20 , 20 , 20 , 20 , 20 , 20 , 10 , 10 } ,
		   { 10 , 10 , 20 , 20 , 20 , 20 , 20 , 20 , 0 , 0 , 20 , 20 , 20 , 20 , 20 , 20 , 20 , 20 , 10 , 10 } ,
		   { 20 , 20 , 20 , 30 , 30 , 30 , 30 , 30 , 0 , 0 , 30 , 30 , 30 , 30 , 30 , 30 , 30 , 20 , 20 , 20 } ,
		   { 30 , 30 , 30 , 40 , 40 , 40 , 40 , 40 , 0 , 0 , 40 , 40 , 40 , 40 , 40 , 40 , 40 , 30 , 30 , 30 } ,
		   { 30 , 30 , 30 , 40 , 40 , 40 , 40 , 40 , 0 , 0 , 40 , 40 , 40 , 40 , 40 , 40 , 40 , 30 , 30 , 30 } ,
		   { 30 , 30 , 30 , 40 , 40 , 40 , 40 , 40 , 0 , 0 , 40 , 40 , 40 , 40 , 40 , 40 , 40 , 30 , 30 , 30 } ,
		   { 30 , 30 , 30 , 40 , 40 , 40 , 40 , 40 , 0 , 0 , 40 , 40 , 40 , 40 , 40 , 40 , 40 , 30 , 30 , 30 } ,
		   { 30 , 30 , 30 , 40 , 40 , 40 , 40 , 40 , 0 , 0 , 40 , 40 , 40 , 40 , 40 , 40 , 40 , 30 , 30 , 30 } ,
		   { 40 , 40 , 40 , 50 , 50 , 50 , 50 , 50 , 0 , 0 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 40 , 40 , 40 } ,
		   { 40 , 40 , 40 , 50 , 50 , 50 , 50 , 50 , 0 , 0 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 40 , 40 , 40 } ,
		   { 40 , 40 , 40 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 40 , 40 , 40 } };
	   public static final double ticketPrices3[][] ={ 
		   { 0 , 0 , 10	, 10 , 10 , 10 , 10 , 10 , 0 , 0 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 0 , 0 } ,
		   { 0 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 0 , 0 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 0 } ,
		   { 10 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 0 , 0 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 10 ,10 } ,
		   { 10 , 10 , 20 , 20 , 20 , 20 , 20 , 20 , 0 , 0 , 20 , 20 , 20 , 20 , 20 , 20 , 20 , 20 , 10 , 10 } ,
		   { 10 , 10 , 20 , 20 , 20 , 20 , 20 , 20 , 0 , 0 , 20 , 20 , 20 , 20 , 20 , 20 , 20 , 20 , 10 , 10 } ,
		   { 10 , 10 , 20 , 20 , 20 , 20 , 20 , 20 , 0 , 0 , 20 , 20 , 20 , 20 , 20 , 20 , 20 , 20 , 10 , 10 } ,
		   { 20 , 20 , 20 , 30 , 30 , 30 , 30 , 30 , 0 , 0 , 30 , 30 , 30 , 30 , 30 , 30 , 30 , 20 , 20 , 20 } ,
		   { 30 , 30 , 30 , 40 , 40 , 40 , 40 , 40 , 0 , 0 , 40 , 40 , 40 , 40 , 40 , 40 , 40 , 30 , 30 , 30 } ,
		   { 30 , 30 , 30 , 40 , 40 , 40 , 40 , 40 , 0 , 0 , 40 , 40 , 40 , 40 , 40 , 40 , 40 , 30 , 30 , 30 } ,
		   { 30 , 30 , 30 , 40 , 40 , 40 , 40 , 40 , 0 , 0 , 40 , 40 , 40 , 40 , 40 , 40 , 40 , 30 , 30 , 30 } ,
		   { 30 , 30 , 30 , 40 , 40 , 40 , 40 , 40 , 0 , 0 , 40 , 40 , 40 , 40 , 40 , 40 , 40 , 30 , 30 , 30 } ,
		   { 30 , 30 , 30 , 40 , 40 , 40 , 40 , 40 , 0 , 0 , 40 , 40 , 40 , 40 , 40 , 40 , 40 , 30 , 30 , 30 } ,
		   { 40 , 40 , 40 , 50 , 50 , 50 , 50 , 50 , 0 , 0 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 40 , 40 , 40 } ,
		   { 40 , 40 , 40 , 50 , 50 , 50 , 50 , 50 , 0 , 0 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 40 , 40 , 40 } ,
		   { 40 , 40 , 40 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 40 , 40 , 40 } };
	   public static final double ticketPrices4[][] ={ 
		   { 0 , 0 , 10	, 10 , 10 , 10 , 10 , 10 , 0 , 0 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 0 , 0 } ,
		   { 0 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 0 , 0 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 0 } ,
		   { 10 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 0 , 0 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 10 ,10 } ,
		   { 10 , 10 , 20 , 20 , 20 , 20 , 20 , 20 , 0 , 0 , 20 , 20 , 20 , 20 , 20 , 20 , 20 , 20 , 10 , 10 } ,
		   { 10 , 10 , 20 , 20 , 20 , 20 , 20 , 20 , 0 , 0 , 20 , 20 , 20 , 20 , 20 , 20 , 20 , 20 , 10 , 10 } ,
		   { 10 , 10 , 20 , 20 , 20 , 20 , 20 , 20 , 0 , 0 , 20 , 20 , 20 , 20 , 20 , 20 , 20 , 20 , 10 , 10 } ,
		   { 20 , 20 , 20 , 30 , 30 , 30 , 30 , 30 , 0 , 0 , 30 , 30 , 30 , 30 , 30 , 30 , 30 , 20 , 20 , 20 } ,
		   { 30 , 30 , 30 , 40 , 40 , 40 , 40 , 40 , 0 , 0 , 40 , 40 , 40 , 40 , 40 , 40 , 40 , 30 , 30 , 30 } ,
		   { 30 , 30 , 30 , 40 , 40 , 40 , 40 , 40 , 0 , 0 , 40 , 40 , 40 , 40 , 40 , 40 , 40 , 30 , 30 , 30 } ,
		   { 30 , 30 , 30 , 40 , 40 , 40 , 40 , 40 , 0 , 0 , 40 , 40 , 40 , 40 , 40 , 40 , 40 , 30 , 30 , 30 } ,
		   { 30 , 30 , 30 , 40 , 40 , 40 , 40 , 40 , 0 , 0 , 40 , 40 , 40 , 40 , 40 , 40 , 40 , 30 , 30 , 30 } ,
		   { 30 , 30 , 30 , 40 , 40 , 40 , 40 , 40 , 0 , 0 , 40 , 40 , 40 , 40 , 40 , 40 , 40 , 30 , 30 , 30 } ,
		   { 40 , 40 , 40 , 50 , 50 , 50 , 50 , 50 , 0 , 0 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 40 , 40 , 40 } ,
		   { 40 , 40 , 40 , 50 , 50 , 50 , 50 , 50 , 0 , 0 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 40 , 40 , 40 } ,
		   { 40 , 40 , 40 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 40 , 40 , 40 } };
	   public static final double ticketPrices5[][] ={ 
		   { 0 , 0 , 10	, 10 , 10 , 10 , 10 , 10 , 0 , 0 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 0 , 0 } ,
		   { 0 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 0 , 0 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 0 } ,
		   { 10 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 0 , 0 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 10 ,10 } ,
		   { 10 , 10 , 20 , 20 , 20 , 20 , 20 , 20 , 0 , 0 , 20 , 20 , 20 , 20 , 20 , 20 , 20 , 20 , 10 , 10 } ,
		   { 10 , 10 , 20 , 20 , 20 , 20 , 20 , 20 , 0 , 0 , 20 , 20 , 20 , 20 , 20 , 20 , 20 , 20 , 10 , 10 } ,
		   { 10 , 10 , 20 , 20 , 20 , 20 , 20 , 20 , 0 , 0 , 20 , 20 , 20 , 20 , 20 , 20 , 20 , 20 , 10 , 10 } ,
		   { 20 , 20 , 20 , 30 , 30 , 30 , 30 , 30 , 0 , 0 , 30 , 30 , 30 , 30 , 30 , 30 , 30 , 20 , 20 , 20 } ,
		   { 30 , 30 , 30 , 40 , 40 , 40 , 40 , 40 , 0 , 0 , 40 , 40 , 40 , 40 , 40 , 40 , 40 , 30 , 30 , 30 } ,
		   { 30 , 30 , 30 , 40 , 40 , 40 , 40 , 40 , 0 , 0 , 40 , 40 , 40 , 40 , 40 , 40 , 40 , 30 , 30 , 30 } ,
		   { 30 , 30 , 30 , 40 , 40 , 40 , 40 , 40 , 0 , 0 , 40 , 40 , 40 , 40 , 40 , 40 , 40 , 30 , 30 , 30 } ,
		   { 30 , 30 , 30 , 40 , 40 , 40 , 40 , 40 , 0 , 0 , 40 , 40 , 40 , 40 , 40 , 40 , 40 , 30 , 30 , 30 } ,
		   { 30 , 30 , 30 , 40 , 40 , 40 , 40 , 40 , 0 , 0 , 40 , 40 , 40 , 40 , 40 , 40 , 40 , 30 , 30 , 30 } ,
		   { 40 , 40 , 40 , 50 , 50 , 50 , 50 , 50 , 0 , 0 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 40 , 40 , 40 } ,
		   { 40 , 40 , 40 , 50 , 50 , 50 , 50 , 50 , 0 , 0 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 40 , 40 , 40 } ,
		   { 40 , 40 , 40 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 40 , 40 , 40 } };
	   public static final double ticketPrices6[][] ={ 
		   { 0 , 0 , 10	, 10 , 10 , 10 , 10 , 10 , 0 , 0 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 0 , 0 } ,
		   { 0 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 0 , 0 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 0 } ,
		   { 10 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 0 , 0 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 10 ,10 } ,
		   { 10 , 10 , 20 , 20 , 20 , 20 , 20 , 20 , 0 , 0 , 20 , 20 , 20 , 20 , 20 , 20 , 20 , 20 , 10 , 10 } ,
		   { 10 , 10 , 20 , 20 , 20 , 20 , 20 , 20 , 0 , 0 , 20 , 20 , 20 , 20 , 20 , 20 , 20 , 20 , 10 , 10 } ,
		   { 10 , 10 , 20 , 20 , 20 , 20 , 20 , 20 , 0 , 0 , 20 , 20 , 20 , 20 , 20 , 20 , 20 , 20 , 10 , 10 } ,
		   { 20 , 20 , 20 , 30 , 30 , 30 , 30 , 30 , 0 , 0 , 30 , 30 , 30 , 30 , 30 , 30 , 30 , 20 , 20 , 20 } ,
		   { 30 , 30 , 30 , 40 , 40 , 40 , 40 , 40 , 0 , 0 , 40 , 40 , 40 , 40 , 40 , 40 , 40 , 30 , 30 , 30 } ,
		   { 30 , 30 , 30 , 40 , 40 , 40 , 40 , 40 , 0 , 0 , 40 , 40 , 40 , 40 , 40 , 40 , 40 , 30 , 30 , 30 } ,
		   { 30 , 30 , 30 , 40 , 40 , 40 , 40 , 40 , 0 , 0 , 40 , 40 , 40 , 40 , 40 , 40 , 40 , 30 , 30 , 30 } ,
		   { 30 , 30 , 30 , 40 , 40 , 40 , 40 , 40 , 0 , 0 , 40 , 40 , 40 , 40 , 40 , 40 , 40 , 30 , 30 , 30 } ,
		   { 30 , 30 , 30 , 40 , 40 , 40 , 40 , 40 , 0 , 0 , 40 , 40 , 40 , 40 , 40 , 40 , 40 , 30 , 30 , 30 } ,
		   { 40 , 40 , 40 , 50 , 50 , 50 , 50 , 50 , 0 , 0 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 40 , 40 , 40 } ,
		   { 40 , 40 , 40 , 50 , 50 , 50 , 50 , 50 , 0 , 0 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 40 , 40 , 40 } ,
		   { 40 , 40 , 40 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 40 , 40 , 40 } };
	   public static final double ticketPrices7[][] ={ 
		   { 0 , 0 , 10	, 10 , 10 , 10 , 10 , 10 , 0 , 0 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 0 , 0 } ,
		   { 0 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 0 , 0 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 0 } ,
		   { 10 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 0 , 0 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 10 , 10 ,10 } ,
		   { 10 , 10 , 20 , 20 , 20 , 20 , 20 , 20 , 0 , 0 , 20 , 20 , 20 , 20 , 20 , 20 , 20 , 20 , 10 , 10 } ,
		   { 10 , 10 , 20 , 20 , 20 , 20 , 20 , 20 , 0 , 0 , 20 , 20 , 20 , 20 , 20 , 20 , 20 , 20 , 10 , 10 } ,
		   { 10 , 10 , 20 , 20 , 20 , 20 , 20 , 20 , 0 , 0 , 20 , 20 , 20 , 20 , 20 , 20 , 20 , 20 , 10 , 10 } ,
		   { 20 , 20 , 20 , 30 , 30 , 30 , 30 , 30 , 0 , 0 , 30 , 30 , 30 , 30 , 30 , 30 , 30 , 20 , 20 , 20 } ,
		   { 30 , 30 , 30 , 40 , 40 , 40 , 40 , 40 , 0 , 0 , 40 , 40 , 40 , 40 , 40 , 40 , 40 , 30 , 30 , 30 } ,
		   { 30 , 30 , 30 , 40 , 40 , 40 , 40 , 40 , 0 , 0 , 40 , 40 , 40 , 40 , 40 , 40 , 40 , 30 , 30 , 30 } ,
		   { 30 , 30 , 30 , 40 , 40 , 40 , 40 , 40 , 0 , 0 , 40 , 40 , 40 , 40 , 40 , 40 , 40 , 30 , 30 , 30 } ,
		   { 30 , 30 , 30 , 40 , 40 , 40 , 40 , 40 , 0 , 0 , 40 , 40 , 40 , 40 , 40 , 40 , 40 , 30 , 30 , 30 } ,
		   { 30 , 30 , 30 , 40 , 40 , 40 , 40 , 40 , 0 , 0 , 40 , 40 , 40 , 40 , 40 , 40 , 40 , 30 , 30 , 30 } ,
		   { 40 , 40 , 40 , 50 , 50 , 50 , 50 , 50 , 0 , 0 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 40 , 40 , 40 } ,
		   { 40 , 40 , 40 , 50 , 50 , 50 , 50 , 50 , 0 , 0 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 40 , 40 , 40 } ,
		   { 40 , 40 , 40 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 50 , 40 , 40 , 40 } };

	   
}


